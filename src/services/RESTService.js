const { REACT_APP_API_URL, REACT_APP_API_START, REACT_APP_API_LIMIT  } = process.env;

export default class RESTService{

    getRange = (start, limit) => {
        let range = '';
        range = start !== undefined ? `?${REACT_APP_API_START}=${start}` : range;
        range = range !== '' && limit !== undefined ? `${range}&${REACT_APP_API_LIMIT}=${limit}` : range;
        return range
    }
    async get(url, start, limit) {
        // Default options are marked with *
        const response = await fetch(REACT_APP_API_URL + url + this.getRange(start, limit), {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        });
        return response.json(); // parses JSON response into native JavaScript objects
    }

    async post(url, data){
        const response = await fetch(REACT_APP_API_URL + url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(data)
        })
        return response.json()
    }

}