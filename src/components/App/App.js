import './App.css';
import Home from '../Home/Home'
import PostView from "../PostView";
import 'semantic-ui-css/semantic.min.css'
import {
    Switch,
    Route,
    withRouter
} from "react-router-dom";
import {useState} from "react";


function App(props) {

    const[current_post, setCurrentPost] = useState();

    const handlePostClick = (post) => {
        setCurrentPost(post)
        props.history.push(`/post/${post.post.id}`)
    }

  return (
    <div className="App">
        <Switch>
            <Route path='/post'>
                <PostView post_view={current_post}/>
            </Route>
            <Route path='/'>
                <Home onPostClick={handlePostClick}/>
            </Route>
        </Switch>
    </div>
  );
}

export default withRouter(App);
