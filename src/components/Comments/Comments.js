import React, { Component } from "react";
import {Button, Form, Comment} from 'semantic-ui-react'
import './Comments.css'

function Comments({comments}){
  return(
      <div className="comments-container">
        <Comment.Group>
          {comments !== undefined ? comments.map(comment =>
              <Comment key={comment.id}>
                <Comment.Avatar src='https://semantic-ui.com/images/avatar/small/matt.jpg'>
                </Comment.Avatar>
                <Comment.Content>
                  <Comment.Author as='a'>{comment.email}</Comment.Author>
                  <Comment.Metadata>
                    <div>Today at 5:42PM</div>
                  </Comment.Metadata>
                  <Comment.Text>{comment.body}</Comment.Text>
                </Comment.Content>
              </Comment>) : null}
        </Comment.Group>

          <Form>
              <Form.TextArea placeholder="Place comment here.."/>
              <Form.Button type="submit" content="Send" icon="edit outline"/>
          </Form>
      </div>
  );
}

export default Comments;
