import React, {useEffect, useState} from "react";
import {Segment, Header, Divider, Button, Icon, Label} from 'semantic-ui-react'
import Comments from "../Comments";
import './PostView.css'

function PostView({post_view}) {

    const {post, comments} = post_view;
    const[open_comments, setOpenComments] = useState(true)
    const [liked, setLiked] = useState(false)

    useEffect(() => {
        setLiked(post.liked)
    }, [post.liked])

    const likeColor = () => {
        return liked === true ? 'blue' : null;
    }

  return(
      <div className='postview-outer'>
          <div>
            <Segment className='postview-container'>
                <p className='postview-user'>{'author ' + post.username}</p>
              <div className='postview-socials'>
                <Button className='postview-social-like' icon='thumbs up outline' color={likeColor()}>
                </Button>
                <Label style={{fontSize: '17px'}}>
                  <Icon name='heart'/>23
                </Label>
              </div>
                <div className='postview-content'>
                    <Header>{post.title}</Header>
                    <Divider/>
                    <p>{post.body}</p>
                </div>

                <Button style={{marginTop: '20px'}}
                        onClick={() => setOpenComments(!open_comments)}
                        floated="right"
                        icon="comment outline"
                        content="Show comments"/>
                <div style={{clear: 'both'}}/>
                {open_comments ? <Comments comments={comments}/> : null}
            </Segment>
          </div>
      </div>
  );
}

export default PostView;
