import React from "react";
import { shallow } from "enzyme";
import PostView from "./PostView";

describe("PostView", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<PostView />);
    expect(wrapper).toMatchSnapshot();
  });
});
