import React, {Component, useEffect, useReducer, useState} from "react";
import { Modal, Button, Form, Message } from 'semantic-ui-react'
import RESTService from "../../../services/RESTService";
import './AddNewPost.css'
import Error from "./Error";
import { getFormValidation, getInputValidation } from "../../Validators/Validators";

const { REACT_APP_API_POST } = process.env;

function formReducer(state, action){
  switch (action.type){
    case 'update-form':
      const { name, value, valid, error, form_valid } = action.data;
      return {
        form: {...state.form, [name]: {value, valid, error}, form_valid},

      };
    default:
      return state;
  }
}

function AddNewPost(props){
  const [open, setOpen] = React.useState(false)
  const rest_service = new RESTService();
  const[saved, setSaved] = useState(false)

  const initial_state = {
    form: {
      user: {value: '', valid: true, error: ''},
      title: {value: '', valid: true, error: ''},
      body: {value: '', valid: true, error: ''},
      form_valid: false
    }
  }

  const[{form}, dispatch] = useReducer(formReducer, initial_state)

  const handleSave = async() => {
    const post = {userId: 1, title: form.title.value, body: form.body.value};

    if(form.form_valid === true){
      const response = await rest_service.post(REACT_APP_API_POST, post)
      setSaved(true);
      setTimeout(() => {
        setSaved(false)
      }, 2000)
    }
  }

  /*useEffect(() => {

    const valid = true;
    const error = '';
    const form_valid = false;

    Object.keys(form).forEach(key => {
      const item = form[key];
      if(item.valid !== undefined){
        dispatch({type: 'update-form', data: {name: key, ...form[key], valid, error, form_valid}});
      }
    })
  }, [open]*/

  const handleChange = (name, value) => {
    const {valid, error} = getInputValidation(name, value);
    let form_valid = getFormValidation(form, name, valid);
    dispatch({type: 'update-form', data: {name, value, valid, error, form_valid}})
  }

  return (
      <Modal
          className="new-post-modal"
          onClose={() => setOpen(false)}
          onOpen={() => setOpen(true)}
          open={open}
          trigger={<Button floated="right" content='new post' icon='plus' labelPosition='right'/>}
      >
        <Modal.Header>Add new post</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Form success>
              <Form.Input error={!form.user.valid} value={form.user.value} label="Username" onChange={e => handleChange('user', e.target.value)}/>
              <Error name={form.user.error}/>
              <Form.Input error={!form.title.valid} value={form.title.value} label="Title" onChange={e => handleChange('title', e.target.value)} />
              <Error name={form.title.error}/>
              <Form.TextArea error={!form.body.valid}  value={form.body.value} onChange={e => handleChange('body', e.target.value)}/>
              <Error name={form.body.error}/>
              <Form.Field>

                {saved === true ?
                  <Message
                    success
                    header='Post has been saved!'
                    list={[
                    'Check post on your website.',
                    ]}
                  /> : null}

              </Form.Field>
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color='black' onClick={() => setOpen(false)}>
            Back
          </Button>
          <Button
              content="Save"
              labelPosition='right'
              icon='checkmark'
              onClick={handleSave}
              positive
          />
        </Modal.Actions>
      </Modal>)
}

export default AddNewPost;
