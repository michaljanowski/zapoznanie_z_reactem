import React from "react";
import { shallow } from "enzyme";
import AddNewPost from "./AddNewPost";

describe("AddNewPost", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<AddNewPost />);
    expect(wrapper).toMatchSnapshot();
  });
});
