import './AddNewPost.css'

function Error({name}){
    return(
        <p className="error">{name}</p>
    )
}

export default Error;