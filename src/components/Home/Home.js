import React, {useEffect, useState} from "react";
import RESTService from "../../services/RESTService";
import Post from './Post/Post'
import { Pagination, Button } from 'semantic-ui-react'
import AddNewPost from "./AddNewPost";
import './Home.css'
const { REACT_APP_API_POST, REACT_APP_API_USERS   } = process.env;

function Home(props){

  const rest_service = new RESTService();
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState([]);
  const [open_add_new_post, setOpenAddNewPost] = useState(false)
  const [posts_count, setPostsCount] = useState();
  const PAGE_RANGE = 5;

  const setPostsUsername = (posts, users) => {
    posts.map(post => {
        const user = users.find(user => user.id === post.userId)
        post.username = user.name;
        return post;
    });
    setPosts([...posts])
  };

  useEffect( async() => {
    let posts = await rest_service.get(REACT_APP_API_POST);
    const users = await rest_service.get(REACT_APP_API_USERS);
    setPostsCount(posts.length);
    setPosts(posts.slice(0, PAGE_RANGE));
    setUsers(users)

    posts = posts.slice(0, PAGE_RANGE);
    setPostsUsername(posts, users);
  }, []);

  const handlePaginationChange = async(e, { activePage }) => {
      const posts = await rest_service.get(REACT_APP_API_POST, (activePage - 1) * PAGE_RANGE, PAGE_RANGE);
      setPostsUsername(posts, users);
  }

  const handlePostClick = (post) => {
      props.onPostClick(post)
  }

  return (
      <div className='home-container'>
          <div>
              <div className="home-posts-container" style={{height: window.innerHeight - 100}}>
                  {posts ?
                      posts.map(post => <Post
                                            onClick={handlePostClick}
                                            key={post.id}
                                            post={post}/>)
                      : null
                  }
              </div>
              <div style={{marginTop: '20px'}}>
                  {posts_count ? <AddNewPost/> : null}
              </div>
              <div style={{clear: 'both'}}/>
              <div className="home-pagination">
                  {posts_count ?
                  <Pagination
                      style={{marginBottom: '50px'}}
                      defaultActivePage={1}
                      firstItem={null}
                      lastItem={null}
                      onPageChange={handlePaginationChange}
                      totalPages={posts_count / PAGE_RANGE} /> : null}
              </div>
          </div>
      </div>
  );
}

export default Home;
