import React, {useState} from "react";
import {Segment, Icon, Label, Divider, Button} from 'semantic-ui-react'
import './Post.css'
import RESTService from "../../../services/RESTService";
import Comments from "../../Comments";
const { REACT_APP_API_POST, REACT_APP_API_COMMENTS   } = process.env;

function Post(props) {

    const {post, onClick} = props;
    const [comments, setComments] = useState()
    const [open_comments, setOpenComments] = useState(false)
    const [liked, setLiked] = useState(false)
    const rest_service = new RESTService();

    const handleClick = async() => {
        let comms = !comments ? await rest_service.get(`${REACT_APP_API_POST}${post.id}${REACT_APP_API_COMMENTS}`) : comments;
        onClick({post: post, comments: comms});
    };

    const handleCommentClick = async() => {
        let comments = await rest_service.get(`${REACT_APP_API_POST}${post.id}${REACT_APP_API_COMMENTS}`);
        setComments(comments)
        setOpenComments(!open_comments)
    }

    const likeColor = () => {
        return liked === true ? 'blue' : null;
    }

    const handleLike = () => {
        setLiked(!liked);
        post.liked = !liked;
    }

  return (
      <Segment className='post-container'>
          <div>
              <Label basic style={{border: 'none'}}>
                  <Icon name='user' /> {post.username}
              </Label>
              <p className='post-time'>{`${Math.floor(Math.random() * (100 - 1 + 1)) + 1} minutes ago`}</p>
          </div>
          <Divider/>

          <div>
              <a className='post-title' onClick={handleClick}>{post.title}</a>
              <p style={{marginTop: '20px'}}>
                  {post.body}
              </p>
          </div>

          <div className='post-button-container'>
              <Button className='post-button' color={likeColor()} onClick={handleLike}>
                  <Icon name='thumbs up outline' />
                  Like
              </Button>
              <Button className='post-button' onClick={handleCommentClick}>
                  <Icon name='comment outline' />
                  Comment
              </Button>
              <Button className='post-button'>
                  <Icon name='share' />
                  Share
              </Button>
          </div>

          {open_comments ? <Comments comments={comments}/> : null}

      </Segment>
  );
}

export default Post;
