export const getInputValidation = (name, value) => {
    let valid = false;
    let error = '';

    switch (name){
        case 'user':
            if(value.trim() === ''){
                valid = false;
                error = 'User field can not be empty.';
            }else{
                valid = true;
                error = '';
            }
            break;
        case 'title':
            if(value.trim() === ''){
                valid = false;
                error = 'Title field can not be empty.';
            }else{
                valid = true;
                error = '';
            }
            break;
        case 'body':
            if(value.trim() === ''){
                valid = false;
                error = 'Body field can not be empty.';
            }else{
                valid = true;
                error = '';
            }
            break;
        default:
            break;
    }
    return {valid, error}
}

export const getFormValidation = (form, name, valid) => {
    let is_valid = true;
    Object.keys(form).forEach(key => {
        const item = form[key];
        if(valid === false){
            is_valid = false;
        }
        if(key !== name && item.valid === false){
            is_valid = false;
        }
    })
    return is_valid;
}